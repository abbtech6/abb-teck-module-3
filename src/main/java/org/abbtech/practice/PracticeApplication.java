package org.abbtech.practice;

import org.abbtech.practice.bean.JavaBeanWithAnnotation;
import org.abbtech.practice.bean.Student;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class PracticeApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext configurableApplicationContext =
				SpringApplication.run(PracticeApplication.class, args);
		JavaBeanWithAnnotation javaBeanWithAnnotation =
				(JavaBeanWithAnnotation) configurableApplicationContext.getBean("JavaBeanWithAnnotation");
		System.out.println(javaBeanWithAnnotation.name);


	}

}
