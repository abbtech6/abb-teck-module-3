package org.abbtech.practice.calculator;

public interface CalculationService

{
    double add(double num1, double num2);

    double subtract(double num1, double num2);

    double multiply(double num1, double num2);

    double divide(double num1, double num2);
}


