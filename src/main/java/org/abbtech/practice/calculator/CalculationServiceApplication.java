package org.abbtech.practice.calculator;

import org.springframework.boot.CommandLineRunner;

public class CalculationServiceApplication implements CommandLineRunner
{
    CalculationServiceApplication(CalculationService calculationService){
        this.calculationService = calculationService;
    }
    private CalculationService calculationService;


    @Override
    public void run(String... args) {
        double num1 = 10;
        double num2 = 5;

        System.out.println("Addition: " + calculationService.add(num1, num2));
        System.out.println("Subtraction: " + calculationService.subtract(num1, num2));
        System.out.println("Multiplication: " + calculationService.multiply(num1, num2));
        System.out.println("Division: " + calculationService.divide(num1, num2));
    }

}
