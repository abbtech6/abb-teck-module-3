package org.abbtech.practice.calculator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/calculation")
public class CalculationController {
    @Autowired
    private CalculationService calculationService;

    @GetMapping("/add")
    public int add(@RequestParam int a, @RequestParam int b) {
        return (int) calculationService.add(a, b);
    }

    @GetMapping("/subtract")
    public int subtract(@RequestParam int a, @RequestParam int b) {
        return (int) calculationService.subtract(a, b);
    }

    @GetMapping("/multiply")
    public int multiply(@RequestParam int a, @RequestParam int b) {
        return (int) calculationService.multiply(a, b);
    }

    @GetMapping("/divide")
    public double divide(@RequestParam int a, @RequestParam int b) {
        return calculationService.divide(a, b);
    }

}
