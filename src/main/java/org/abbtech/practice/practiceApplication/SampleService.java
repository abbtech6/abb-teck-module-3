package org.abbtech.practice.practiceApplication;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sample")
public class SampleService
{
    @GetMapping("/getSample")
    public void getSample()
    {
        System.out.printf("first sample");
    }
}
