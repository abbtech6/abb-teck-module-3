package org.abbtech.practice.Task19.Controller;

import org.abbtech.practice.Task19.Model.Student;
import org.abbtech.practice.Task19.Service.StudentService;
import org.abbtech.practice.Task19.Service.StudentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/student")
public class StudentController {
      private    StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @PostMapping("/create")
    public Student createStudent(@RequestBody Student student) {
        return studentService.save(student);
    }

    @GetMapping("/{id}")
    public List<Student> getStudentByID(@PathVariable int id) {
        return studentService.findStudentById(id);
    }
}

