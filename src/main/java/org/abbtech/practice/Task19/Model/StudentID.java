package org.abbtech.practice.Task19.Model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;

@Entity
public class StudentID
{
    @Id
    @GeneratedValue
    private int cardID;
    private String expiryDate;
    @OneToOne
    private Student student;

    public StudentID(int cardID, String expiryDate) {
        this.cardID = cardID;
        this.expiryDate = expiryDate;
    }

    public StudentID() {
    }

    public int getCardID() {
        return cardID;
    }

    public void setCardID(int cardID) {
        this.cardID = cardID;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }
}