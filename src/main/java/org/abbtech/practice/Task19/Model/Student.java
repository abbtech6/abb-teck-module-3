package org.abbtech.practice.Task19.Model;

import jakarta.persistence.*;
import lombok.Getter;

import java.util.List;

@Entity
@Table(name = "students")
public class Student
{
    @Getter
    @Id
    @GeneratedValue
    private int id;
    private String name;
    private String lastName;
    private int cardID;
    @ManyToMany
    @JoinTable(name = "student_class",
                 joinColumns = @JoinColumn(name = "student_id"),
                 inverseJoinColumns = @JoinColumn(name = "class_id"))
    private List<Classes> classes;
    @OneToOne
    private StudentID studentID;

    public Student(int id, String name, String lastName, int cardID) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.cardID = cardID;

    }

    public Student() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getCardID() {
        return cardID;
    }

    public void setCardID(int cardID) {
        this.cardID = cardID;
    }
}
