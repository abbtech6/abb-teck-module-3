package org.abbtech.practice.Task19.Model;

import jakarta.persistence.*;

import java.util.List;

@Entity
public class Classes
{
    @Id
    @GeneratedValue
    private  int classID;
    private String className;
    @ManyToMany(mappedBy = "classes")
    private List<Student> studentList;

    public Classes(int classID, String className) {
        this.classID = classID;
        this.className = className;
    }

    public Classes() {
    }

    public int getClassID() {
        return classID;
    }

    public void setClassID(int classID) {
        this.classID = classID;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
