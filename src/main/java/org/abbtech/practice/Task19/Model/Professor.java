package org.abbtech.practice.Task19.Model;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "teachers")
public class Professor
{
    @Id
    @GeneratedValue
    private int professorID;
    private String name;
    private String lastName;
    private int departmentID;


    @ManyToOne
    private Department department;


    public Professor(int professorID, String name, String lastName, int departmentID) {
        this.professorID = professorID;
        this.name = name;
        this.lastName = lastName;
        this.departmentID = departmentID;

    }

    public Professor() {
    }

    public int getProfessorID() {
        return professorID;
    }

    public void setProfessorID(int professorID) {
        this.professorID = professorID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getDepartmentID() {
        return departmentID;
    }

    public void setDepartmentID(int departmentID) {
        this.departmentID = departmentID;
    }


}
