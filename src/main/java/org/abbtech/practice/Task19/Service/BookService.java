package org.abbtech.practice.Task19.Service;

import org.abbtech.practice.Task19.Model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BookService extends JpaRepository<Book,Integer>
{
    Book save(Book book);
    List<Book> findBookByBookID(int id);
    List<Book> findAll();
    List<Book> findBooksByTitle(String title);
    List<Book> findBooksByAuthor(String author);
    void deleteByBookID(int id);
    void deleteBookByAuthor(String author);

}
