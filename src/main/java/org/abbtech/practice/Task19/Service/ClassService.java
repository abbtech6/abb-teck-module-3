package org.abbtech.practice.Task19.Service;

import org.abbtech.practice.Task19.Model.Classes;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClassService extends JpaRepository<Classes,Integer>
{
    Classes save(Classes classes);
    List<Classes> findClassesByClassID(int id);
    List<Classes> findClassesByClassName(String name);
    void deleteByClassID(int id);

}
