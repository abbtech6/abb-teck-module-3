package org.abbtech.practice.Task19.Service;

import org.abbtech.practice.Task19.Model.StudentID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface StudentIDService extends CrudRepository<StudentID,Integer>
{
    StudentID save(StudentID studentID);
    List<StudentID> findStudentIDByCardID(int id);
    List<StudentID> findStudentIDByExpiryDate(String date);
    void deleteByCardID(int id);

}
