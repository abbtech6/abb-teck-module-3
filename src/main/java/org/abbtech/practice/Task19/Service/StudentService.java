package org.abbtech.practice.Task19.Service;

import org.abbtech.practice.Task19.Model.Student;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

public  interface StudentService
{
    List<Student> findStudentByName(String name);
    List<Student> findStudentById(int id);
    List<Student> findStudentByCardID(int id);
    void deleteByCardID(int id);
    Student save(Student student);
    List<Student> findAll();

}
