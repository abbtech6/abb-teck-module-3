package org.abbtech.practice.Task19.Service;

import org.abbtech.practice.Task19.Model.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DepartmentService extends JpaRepository<Department,Integer>
{
    Department save(Department department);
    List<Department> findDepartmentByDepartmentID(int id);

    void deleteByDepartmentID(int id);
    void  deleteByDepartmentName(String name);

}
