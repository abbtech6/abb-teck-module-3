package org.abbtech.practice.Task19.Service;

import lombok.RequiredArgsConstructor;
import org.abbtech.practice.Task19.Model.Student;
import org.abbtech.practice.Task19.Repositories.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
@Service
@RequiredArgsConstructor
public  class StudentServiceImpl implements StudentService
{
     private final StudentRepository studentService;

    public List<Student> findStudentById(int id) {
        return studentService.findStudentById(id);
    }

    public List<Student> findStudentByName(String name) {
        return studentService.findStudentByName(name);
    }


    public List<Student> findStudentByCardID(int id) {
        return studentService.findStudentByCardID(id);
    }


    public void deleteByCardID(int id) {
         studentService.deleteByCardID(id);
    }


    public Student save(Student student) {
        return studentService.save(student);
    }


    public List<Student> findAll() {
        return studentService.findAll();
    }



}
