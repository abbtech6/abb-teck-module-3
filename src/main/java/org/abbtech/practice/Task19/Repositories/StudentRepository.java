package org.abbtech.practice.Task19.Repositories;

import org.abbtech.practice.Task19.Model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<Student,Integer> {
    List<Student> findStudentByName(String name);
    List<Student> findStudentById(int id);
    List<Student> findStudentByCardID(int id);
    void deleteByCardID(int id);
    Student save(Student student);
    List<Student> findAll();
}
