package org.abbtech.practice.Task19.Repositories;

import org.abbtech.practice.Task19.Model.Professor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProfessorRepository extends JpaRepository<Professor,Integer> {
}
