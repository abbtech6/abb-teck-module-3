package org.abbtech.practice.bean;

import org.springframework.stereotype.Component;

@Component(value = "JavaBeanWithAnnotation")
public class JavaBeanWithAnnotation
{
    public String name = "Value";
}

