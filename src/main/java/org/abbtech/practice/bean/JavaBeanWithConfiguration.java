package org.abbtech.practice.bean;

import org.abbtech.practice.bean.Student;
import org.springframework.context.annotation.Bean;

public class JavaBeanWithConfiguration
{
    @Bean("Stundent1")
    public Student getStudent() {
        Student student = new Student();
        student.name = "Value";
        return student;
    }

    @Bean("Stundent12")
    public Student getStudent1() {
        Student student = new Student();
        student.name = "Value1";
        return student;
    }

}
