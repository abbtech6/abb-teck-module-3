package org.abbtech.practice.bean;

import org.abbtech.practice.bean.NotificationService;

public class MainNotificationService implements NotificationService {
    @Override
    public void sendNotification() {
        System.out.println("send mail notification");
    }

}
