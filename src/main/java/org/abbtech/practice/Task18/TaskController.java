package org.abbtech.practice.Task18;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/tasks")
public class TaskController {
    private final TaskServiceImpl taskService;

        @Autowired
        public TaskController(TaskServiceImpl taskService) {
            this.taskService = taskService;
        }

        @PostMapping("/create")
        public Task createTask(@RequestBody Task task) {
            return taskService.save(task);
        }

        @GetMapping("/get/{id}")
        public ResponseEntity<Task> getByid(@PathVariable int id) {
            Task task = taskService.getTaskById(id);
            if (task != null) {
                return ResponseEntity.ok(task);
            } else {
                return ResponseEntity.notFound().build();
            }
        }
    }

