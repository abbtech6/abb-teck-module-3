package org.abbtech.practice.Task18;

import java.util.List;

public interface TaskService {
    Task getTaskById(Integer id);
    List<Task> getAllTasks();
    Task save(Task task);
    Task createTask(Task task);
    void deleteById(Integer id);
}
